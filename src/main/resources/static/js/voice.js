$(document).ready()
{
	Highcharts.setOptions({
		chart: {
			style: {
				fontFamily: 'Verdana'
			}
		},
		drilldown: {
			activeDataLabelStyle: {
				textDecoration: 'none'
			}
		}
	});
	$.ajax({
		type : "GET",
		url : "voiceByServer",
		success: function(data)
		{
			for(var i = 0; i < data.plotBands.length; i++)
			{
				/* Handles getting how deep into the graph we are for plot bands. */
				data.plotBands[i].from = moment(data.plotBands[i].from).diff(moment(data.dates[0]), 'seconds')/86400
				data.plotBands[i].to = moment(data.plotBands[i].to).diff(moment(data.dates[0]), 'seconds')/86400
			}

			// Create the chart
			Highcharts.chart('voiceByServer', {
                chart: {
                    type: 'areaspline',
                    zoomType: 'x'
                },
                title: {
                    text: 'Voice Chat'
                },
                xAxis: {
                    categories: data.dates,
                    plotBands: data.plotBands
                },
                yAxis:
				{
					title:
					{
						text: "Hours"
					},
					labels:
					{
						formatter: function(){return this.value;}
					},
					ceiling: 24,
					min: 0,
					max: 24
				},
                tooltip:
				{
					shared: true,
					valueSuffix: " hours",
					formatter: function () {
						var ttString = "<b>" + this.x + "</b>" + "<br/><br/>"

						for(var i = 0; i < this.points.length; i++)
						{
							var inMinutes = Math.floor(this.points[i].y*60);
                            var totalHours = Math.floor(inMinutes / 60);
                            var totalMinutes = (inMinutes%60);

							if(i != 0)
							{
								ttString += "<br/>"
							}

							ttString += "<b>" + this.points[i].series.name + "</b>: " + totalHours + (totalHours == 1 ? " hour " : " hours ") + (totalMinutes == 0 ? "" : (totalMinutes + (totalMinutes == 1 ? " minute " : " minutes ")));
						}

						return ttString
					}
				},
                credits: {
                    enabled: false
                },
                plotOptions:
				{
					series: {
						events: {
							legendItemClick: function () {
								var seriesIndex = this.index;
								var series = this.chart.series;
								if (this.visible && this.chart.restIsHidden) {
								  for (var i = 0; i < series.length; i++) {
									if (series[i].index != seriesIndex) {
									  series[i].show();
									}
								  }
								  this.chart.restIsHidden = false;
								} else {
								  for (var i = 0; i < series.length; i++) {
									if (series[i].index != seriesIndex) {
									  series[i].hide();
									}
								  }
								  this.show()
								  this.chart.restIsHidden = true;
								}
								return false;
							}
						},
						states: {
							inactive: {
								opacity: 0.025
							}
						}
					}
				},
                series: data.series
            });
		}
	});
}