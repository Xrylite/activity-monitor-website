$(document).ready()
{
	Highcharts.setOptions({
		chart: {
			style: {
				fontFamily: 'Verdana'
			}
		},
		drilldown: {
			activeDataLabelStyle: {
				textDecoration: 'none'
			}
		}
	});

	$.ajax({
		type : "GET",
		url : "totalMessagesForAllChannels",
		success: function(data)
		{
			// Create the chart
			Highcharts.chart('messagesByChannel', {
				chart: {
					type: 'pie'
				},
				title: {
					text: 'Total Messages by Channel'
				},
				plotOptions: {
					series: {
						dataLabels: {
							enabled: true,
							format: '{point.name}: {point.y}'
						}
					}
				},
				credits: {
					enabled: false
				},
				series: data.series,
				drilldown: data.drilldown
			});
		}
	});

	$.ajax({
		type : "GET",
		url : "totalMessagesForAllUsers",
		success: function(data)
		{
			// Create the chart
			Highcharts.chart('messagesByUser', {
				chart: {
					type: 'pie'
				},
				title: {
					text: 'Total Messages by User'
				},
				plotOptions: {
					series: {
						dataLabels: {
							enabled: true,
							format: '{point.name}: {point.y}'
						}
					}
				},
				credits: {
					enabled: false
				},
				series: data.series,
				drilldown: data.drilldown
			});
		}
	});

	$.ajax({
		type : "GET",
		url : "messagesByServer",
		success: function(data)
		{
			// Create the chart
			Highcharts.chart('messagesByServer', {
				chart: {
					type: 'areaspline',
					zoomType: 'x'
				},
				title: {
					text: 'Messages Over Time'
				},
				xAxis: {
					categories: data.dates
				},
				yAxis:
				{
					title:
					{
						text: "Messages"
					},
					labels:
					{
						formatter: function(){return this.value;}
					}
				},
				tooltip:
				{
					shared: true,
					valueSuffix: " messages",
					formatter: function () {
						var ttString = "<b>" + this.x + "</b>" + "<br/><br/>"

						for(var i = 0; i < this.points.length; i++)
						{
							if(i != 0)
							{
								ttString += "<br/>"
							}

							ttString += "<b>" + this.points[i].series.name + "</b>: " + this.points[i].y;
						}

						return ttString
					}
				},
				credits: {
					enabled: false
				},
				plotOptions:
				{
					series: {
						events: {
							legendItemClick: function () {
								var seriesIndex = this.index;
								var series = this.chart.series;
								if (this.visible && this.chart.restIsHidden) {
								  for (var i = 0; i < series.length; i++) {
									if (series[i].index != seriesIndex) {
									  series[i].show();
									}
								  }
								  this.chart.restIsHidden = false;
								} else {
								  for (var i = 0; i < series.length; i++) {
									if (series[i].index != seriesIndex) {
									  series[i].hide();
									}
								  }
								  this.show()
								  this.chart.restIsHidden = true;
								}
								return false;
							}
						},
						states: {
							inactive: {
								opacity: 0.025
							}
						}
					}
				},
				series: data.series
			});
		}
	});
}