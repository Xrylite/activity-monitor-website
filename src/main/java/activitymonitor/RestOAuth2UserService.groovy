package activitymonitor

import activitymonitor.postgres.MemberRoleAccess
import org.springframework.beans.factory.annotation.Value
import org.springframework.core.ParameterizedTypeReference
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpMethod
import org.springframework.http.ResponseEntity
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.oauth2.client.userinfo.OAuth2UserRequest
import org.springframework.security.oauth2.client.userinfo.OAuth2UserService
import org.springframework.security.oauth2.core.OAuth2AuthenticationException
import org.springframework.security.oauth2.core.user.DefaultOAuth2User
import org.springframework.security.oauth2.core.user.OAuth2User
import org.springframework.security.oauth2.core.user.OAuth2UserAuthority
import org.springframework.stereotype.Component
import org.springframework.web.client.RestOperations

@Component
class RestOAuth2UserService implements OAuth2UserService<OAuth2UserRequest, OAuth2User>
{
	private static String STATIC_ADMIN_ID

	@Value('${discord.admin.user.id}')
	void setAdminId(String adminId)
	{
		STATIC_ADMIN_ID = adminId
	}

	private RestOperations restOperations

	RestOAuth2UserService(RestOperations restOperations)
	{
		this.restOperations = restOperations
	}

	OAuth2User loadUser(OAuth2UserRequest userRequest) throws OAuth2AuthenticationException
	{
		String userInfoUrl = userRequest.getClientRegistration().getProviderDetails().getUserInfoEndpoint().getUri()
		HttpHeaders headers = new HttpHeaders()
		headers.set(HttpHeaders.AUTHORIZATION, String.format("Bearer %s", userRequest.getAccessToken().getTokenValue()))
		headers.set(HttpHeaders.USER_AGENT, "DiscordBot (https://activitymonitor.live)")
		ParameterizedTypeReference<Map<String, Object>> typeReference = new ParameterizedTypeReference<Map<String, Object>>(){
		}
		ResponseEntity<Map<String, Object>> responseEntity = restOperations.exchange(userInfoUrl, HttpMethod.GET, new HttpEntity<>(headers), typeReference)
		Map<String, Object> userAttributes = responseEntity.getBody()
//		Set<GrantedAuthority> authorities = Collections.singleton(new OAuth2UserAuthority(userAttributes))
		Set<GrantedAuthority> authorities = new HashSet<GrantedAuthority>()

		if(userAttributes.id == STATIC_ADMIN_ID)
		{
			authorities.add(new OAuth2UserAuthority("ROLE_ADMIN", userAttributes))
		}

		for(String role : MemberRoleAccess.getRolesByUserId(userAttributes.id))
		{
			authorities.add(new OAuth2UserAuthority("ROLE_" + role.replaceAll(" ", "_").toUpperCase(), userAttributes))
		}

		return new DefaultOAuth2User(authorities, userAttributes, userRequest.getClientRegistration().getProviderDetails().getUserInfoEndpoint().getUserNameAttributeName())
	}
}