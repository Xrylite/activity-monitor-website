package activitymonitor

import de.dentrassi.crypto.pem.PemKeyStoreProvider
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import java.security.Security

@SpringBootApplication
class ActivityMonitorApplication
{
	static void main(String[] args)
	{
		Security.addProvider(new PemKeyStoreProvider())
		SpringApplication.run(ActivityMonitorApplication.class, args)
	}
}