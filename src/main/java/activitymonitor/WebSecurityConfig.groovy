package activitymonitor

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.web.client.RestOperations
import org.springframework.web.client.RestTemplate

@Configuration
class WebSecurityConfig extends WebSecurityConfigurerAdapter
{
	@Override
	protected void configure(HttpSecurity http) throws Exception
	{
		http.authorizeRequests()
				.antMatchers("/", "/css/*", "/js/*").permitAll()
				.anyRequest().authenticated()
				.and()
				.oauth2Login()
				.tokenEndpoint().accessTokenResponseClient(new RestOAuth2AccessTokenResponseClient(restOperations()))
				.and()
				.userInfoEndpoint().userService(new RestOAuth2UserService(restOperations()))
	}

	@Bean
	RestOperations restOperations()
	{
		return new RestTemplate()
	}
}
