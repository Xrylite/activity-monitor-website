package activitymonitor.highcharts.logic

import activitymonitor.highcharts.model.*
import activitymonitor.highcharts.postgres.VoiceAccess
import java.time.LocalDate

class TotalVoice
{
	static HCAreaSplineStructure getTotalVoiceForAllUsers()
	{
		HCAreaSplineStructure hcass = new HCAreaSplineStructure()
		hcass.dates = new ArrayList<LocalDate>()

		List<TotalVoiceDurations> listTvd = VoiceAccess.getAllVoiceDurations()

		Map<String, ArrayList<Double>> mapData = new HashMap<String, ArrayList<Double>>()

		for(TotalVoiceDurations tvd : listTvd)
		{
			hcass.dates.add(tvd.date)

			for(UserVoiceDuration uvd : tvd.listVoiceDurations)
			{
				if(!mapData.containsKey(uvd.userName))
				{
					List<Double> tst = new ArrayList<Double>()
					tst.add(uvd.voiceHours)
					mapData.put(uvd.userName, tst)
				}
				else
				{
					mapData.get(uvd.userName).add(uvd.voiceHours)
				}
			}
		}

		for(String key : mapData.keySet())
		{
			List<Double> listHours = mapData.get(key)

			for(int i = 1; i < listHours.size() - 1; i++)
			{
				if((listHours.get(i-1) == null || listHours.get(i-1) == 0) && listHours.get(i) == 0 && listHours.get(i+1) == 0)
				{
					listHours.set(i, null)
				}
			}

			if(listHours.size() >= 1 && listHours.get(0) == 0)
			{
				listHours.set(0, null)
			}

			if(listHours.size() >= 1 && listHours.get(listHours.size()-1) == 0)
			{
				listHours.set(listHours.size()-1, null)
			}
		}

		List<HCSeries> listHcs = new ArrayList<HCSeries>()

		for(String key : mapData.keySet())
		{
			HCSeries hcs = new HCSeries()
			hcs.name = key

			List<HCData> listData = new ArrayList<HCData>()

			for(Double d : mapData.get(key))
			{
				listData.add(new HCData(d))
			}

			hcs.name = key
			hcs.data = listData

			listHcs.add(hcs)
		}

		hcass.series = listHcs

		/* Plot bands */
		List<TimeRange> outages = VoiceAccess.allOfflineInstances

		List<HCPlotBand> listPlotBand = new ArrayList<HCPlotBand>()

		for(TimeRange tr : outages)
		{
			HCPlotBand hcpb = new HCPlotBand()
			hcpb.from = tr.startTime
			hcpb.to = tr.endTime
			hcpb.color = "rgba(255, 00, 00, 0.33)"

			listPlotBand.add(hcpb)
		}

		hcass.plotBands = listPlotBand

		return hcass
	}
}