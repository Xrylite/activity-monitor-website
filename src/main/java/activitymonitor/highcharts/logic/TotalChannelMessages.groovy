package activitymonitor.highcharts.logic

import activitymonitor.highcharts.model.*
import activitymonitor.highcharts.postgres.MessagesAccess

class TotalChannelMessages
{
	static HCPieStructure getTotalMessagesForAllChannels()
	{
		HCPieStructure hcs = new HCPieStructure()

		List<ChannelTotalMessages> listCtm = MessagesAccess.getAllTotalMessagesByChannel()

		HCSeries series = new HCSeries()
		List<HCData> listData = new ArrayList<HCData>()
		series.name = 'Messages'

		for(ChannelTotalMessages ctm : listCtm)
		{
			listData.add(new HCData(ctm.channelName, ctm.totalMessages, ctm.channelId))
		}

		series.data = listData

		HCSeriesWrapper wrappedDrilldownSeries = new HCSeriesWrapper()
		wrappedDrilldownSeries.series = new ArrayList<HCSeries>()

		for(ChannelTotalMessages ctm : listCtm)
		{
			HCSeries drilldownSeries = new HCSeries()
			drilldownSeries.id = ctm.channelId
			drilldownSeries.name = ctm.channelName

			List<UserTotalMessages> listUtm = MessagesAccess.getUserTotalMessagesByChannel(ctm.channelId)
			List<HCData> listDrilldownData = new ArrayList<HCData>()

			for(UserTotalMessages utm : listUtm)
			{
				HCData drilldownData = new HCData()
				drilldownData.name = utm.userName
				drilldownData.y = utm.totalMessages
				drilldownData.drilldown = null

				listDrilldownData.add(drilldownData)
			}

			drilldownSeries.data = listDrilldownData

			wrappedDrilldownSeries.series.add(drilldownSeries)
		}

		hcs.series = Arrays.asList(series)
		hcs.drilldown = wrappedDrilldownSeries

		return hcs
	}
}