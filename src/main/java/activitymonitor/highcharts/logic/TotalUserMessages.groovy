package activitymonitor.highcharts.logic

import activitymonitor.highcharts.model.*
import activitymonitor.highcharts.postgres.MessagesAccess

import java.time.LocalDate

class TotalUserMessages
{
	static HCPieStructure getTotalMessagesForAllUsers()
	{
		HCPieStructure hcs = new HCPieStructure()

		List<UserTotalMessages> listUtm = MessagesAccess.getAllTotalMessagesByUser()

		HCSeries series = new HCSeries()
		List<HCData> listData = new ArrayList<HCData>()
		series.name = 'Messages'

		for(UserTotalMessages utm : listUtm)
		{
			listData.add(new HCData(utm.userName, utm.totalMessages, utm.userId))
		}

		series.data = listData

		HCSeriesWrapper wrappedDrilldownSeries = new HCSeriesWrapper()
		wrappedDrilldownSeries.series = new ArrayList<HCSeries>()

		for(UserTotalMessages utm : listUtm)
		{
			HCSeries drilldownSeries = new HCSeries()
			drilldownSeries.id = utm.userId
			drilldownSeries.name = utm.userName
			List<ChannelTotalMessages> listCtm = MessagesAccess.getChannelTotalMessagesByUser(utm.userId)
			List<HCData> listDrilldownData = new ArrayList<HCData>()

			for(ChannelTotalMessages ctm : listCtm)
			{
				HCData drilldownData = new HCData()
				drilldownData.name = ctm.channelName
				drilldownData.y = ctm.totalMessages
				drilldownData.drilldown = null

				listDrilldownData.add(drilldownData)
			}

			drilldownSeries.data = listDrilldownData

			wrappedDrilldownSeries.series.add(drilldownSeries)
		}

		hcs.series = Arrays.asList(series)
		hcs.drilldown = wrappedDrilldownSeries

		return hcs
	}

	static HCAreaSplineStructure getTotalMessagesForServer()
	{
		HCAreaSplineStructure hcass = new HCAreaSplineStructure()
		hcass.dates = new ArrayList<LocalDate>()

		List<TotalMessageCounts> listTmc = MessagesAccess.getAllTotalMessages()

		Map<String, ArrayList<Integer>> mapData = new HashMap<String, ArrayList<Integer>>()

		for(TotalMessageCounts tmc : listTmc)
		{
			hcass.dates.add(tmc.date)

			for(UserTotalMessages utm : tmc.listTotalMessages)
			{
				if(!mapData.containsKey(utm.userName))
				{
					List<Integer> tst = new ArrayList<Integer>()
					tst.add(utm.totalMessages)
					mapData.put(utm.userName, tst)
				}
				else
				{
					mapData.get(utm.userName).add(utm.totalMessages)
				}
			}
		}

		for(String key : mapData.keySet())
		{
			List<Integer> listMessages = mapData.get(key)

			for(int i = 1; i < listMessages.size() - 1; i++)
			{
				if((listMessages.get(i-1) == null || listMessages.get(i-1) == 0) && listMessages.get(i) == 0 && listMessages.get(i+1) == 0)
				{
					listMessages.set(i, null)
				}
			}

			if(listMessages.size() >= 1 && listMessages.get(0) == 0)
			{
				listMessages.set(0, null)
			}

			if(listMessages.size() >= 1 && listMessages.get(listMessages.size()-1) == 0)
			{
				listMessages.set(listMessages.size()-1, null)
			}
		}

		List<HCSeries> listHcs = new ArrayList<HCSeries>()

		for(String key : mapData.keySet())
		{
			HCSeries hcs = new HCSeries()
			hcs.name = key

			List<HCData> listData = new ArrayList<HCData>()

			for(Double d : mapData.get(key))
			{
				listData.add(new HCData(d))
			}

			hcs.name = key
			hcs.data = listData

			listHcs.add(hcs)
		}

		hcass.series = listHcs

		return hcass
	}
}