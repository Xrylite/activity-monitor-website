package activitymonitor.highcharts.postgres

import activitymonitor.highcharts.model.ChannelTotalMessages
import activitymonitor.highcharts.model.TotalMessageCounts
import activitymonitor.highcharts.model.UserTotalMessages
import activitymonitor.postgres.ConnectionSource

import java.sql.Connection
import java.sql.PreparedStatement
import java.sql.ResultSet
import java.sql.SQLException

class MessagesAccess
{
	static List<ChannelTotalMessages> getAllTotalMessagesByChannel()
	{
		List<ChannelTotalMessages> listCtm = new ArrayList<ChannelTotalMessages>()

		try(Connection conn = ConnectionSource.getConnection())
		{
			final String selectSQL = "SELECT rslt.text_channel_id, rslt.text_channel_name, rslt.total_channel_messages FROM (SELECT DISTINCT atc.text_channel_id, atc.text_channel_name, COUNT(*) OVER(PARTITION BY ac.category_name, atc.text_channel_name) total_channel_messages FROM am_message am INNER JOIN am_user au ON au.user_id = am.user_id INNER JOIN am_member ame ON ame.user_id = au.user_id INNER JOIN am_text_channel atc ON atc.text_channel_id = am.text_channel_id INNER JOIN am_category ac ON ac.category_id = atc.category_id WHERE atc.text_channel_deleted = false AND ac.category_id != '783459663735291914' AND au.user_bot = false AND ame.member_left = false) rslt ORDER BY rslt.total_channel_messages DESC"

			try(PreparedStatement pStatement = conn.prepareStatement(selectSQL))
			{
				try(ResultSet rSet = pStatement.executeQuery())
				{
					if(rSet.isBeforeFirst())
					{
						while(rSet.next())
						{
							listCtm.add(new ChannelTotalMessages(rSet.getString(1), rSet.getString(2), rSet.getInt(3)))
						}
					}
					else
					{
						System.out.println("{getAllTotalMessagesByChannel}: No data returned.")
					}
				}
			}
		}
		catch(SQLException e)
		{
			e.printStackTrace()
		}

		return listCtm
	}

	static List<UserTotalMessages> getUserTotalMessagesByChannel(String channelId)
	{
		List<UserTotalMessages> listUtm = new ArrayList<UserTotalMessages>()

		try(Connection conn = ConnectionSource.getConnection())
		{
			final String selectSQL = "SELECT rslt.user_id, rslt.display_name, rslt.total_channel_messages FROM (SELECT DISTINCT au.user_id, COALESCE(ame.member_nickname, au.user_username) display_name, COUNT(*) OVER(PARTITION BY ac.category_name, atc.text_channel_name, COALESCE(ame.member_nickname, au.user_username)) total_channel_messages FROM am_message am INNER JOIN am_user au ON au.user_id = am.user_id INNER JOIN am_member ame ON ame.user_id = au.user_id INNER JOIN am_text_channel atc ON atc.text_channel_id = am.text_channel_id INNER JOIN am_category ac ON ac.category_id = atc.category_id WHERE atc.text_channel_deleted = false AND ac.category_id != '783459663735291914' AND au.user_bot = false AND ame.member_left = false AND atc.text_channel_id = ?) rslt ORDER BY rslt.total_channel_messages DESC, rslt.display_name"

			try(PreparedStatement pStatement = conn.prepareStatement(selectSQL))
			{
				pStatement.setString(1, channelId)

				try(ResultSet rSet = pStatement.executeQuery())
				{
					if(rSet.isBeforeFirst())
					{
						while(rSet.next())
						{
							listUtm.add(new UserTotalMessages(rSet.getString(1), rSet.getString(2), rSet.getInt(3)))
						}
					}
					else
					{
						System.out.println("{getUserTotalMessagesByChannel}: No data returned.")
					}
				}
			}
		}
		catch(SQLException e)
		{
			e.printStackTrace()
		}

		return listUtm
	}

	static List<UserTotalMessages> getAllTotalMessagesByUser()
	{
		List<UserTotalMessages> listUtm = new ArrayList<UserTotalMessages>()

		try(Connection conn = ConnectionSource.getConnection())
		{
			final String selectSQL = "SELECT rslt.user_id, rslt.display_name, rslt.total_channel_messages FROM (SELECT DISTINCT au.user_id, COALESCE(ame.member_nickname, au.user_username) display_name, COUNT(*) OVER(PARTITION BY COALESCE(ame.member_nickname, au.user_username)) total_channel_messages FROM am_message am INNER JOIN am_user au ON au.user_id = am.user_id INNER JOIN am_member ame ON ame.user_id = au.user_id INNER JOIN am_text_channel atc ON atc.text_channel_id = am.text_channel_id INNER JOIN am_category ac ON ac.category_id = atc.category_id WHERE atc.text_channel_deleted = false AND ac.category_id != '783459663735291914' AND au.user_bot = false AND ame.member_left = false) rslt ORDER BY rslt.total_channel_messages DESC, rslt.display_name"

			try(PreparedStatement pStatement = conn.prepareStatement(selectSQL))
			{
				try(ResultSet rSet = pStatement.executeQuery())
				{
					if(rSet.isBeforeFirst())
					{
						while(rSet.next())
						{
							listUtm.add(new UserTotalMessages(rSet.getString(1), rSet.getString(2), rSet.getInt(3)))
						}
					}
					else
					{
						System.out.println("{getAllTotalMessagesByUser}: No data returned.")
					}
				}
			}
		}
		catch(SQLException e)
		{
			e.printStackTrace()
		}

		return listUtm
	}

	static List<ChannelTotalMessages> getChannelTotalMessagesByUser(String userId)
	{
		List<ChannelTotalMessages> listCtm = new ArrayList<ChannelTotalMessages>()

		try(Connection conn = ConnectionSource.getConnection())
		{
			final String selectSQL = "SELECT rslt.text_channel_id, rslt.text_channel_name, rslt.total_channel_messages FROM (SELECT DISTINCT atc.text_channel_id, atc.text_channel_name, COUNT(*) OVER(PARTITION BY ac.category_name, atc.text_channel_name) total_channel_messages FROM am_message am INNER JOIN am_user au ON au.user_id = am.user_id INNER JOIN am_member ame ON ame.user_id = au.user_id INNER JOIN am_text_channel atc ON atc.text_channel_id = am.text_channel_id INNER JOIN am_category ac ON ac.category_id = atc.category_id WHERE atc.text_channel_deleted = false AND ac.category_id != '783459663735291914' AND au.user_id = ? AND au.user_bot = false AND ame.member_left = false) rslt ORDER BY rslt.total_channel_messages DESC"

			try(PreparedStatement pStatement = conn.prepareStatement(selectSQL))
			{
				pStatement.setString(1, userId)

				try(ResultSet rSet = pStatement.executeQuery())
				{
					if(rSet.isBeforeFirst())
					{
						while(rSet.next())
						{
							listCtm.add(new ChannelTotalMessages(rSet.getString(1), rSet.getString(2), rSet.getInt(3)))
						}
					}
					else
					{
						System.out.println("{getChannelTotalMessagesByUser}: No data returned.")
					}
				}
			}
		}
		catch(SQLException e)
		{
			e.printStackTrace()
		}

		return listCtm
	}

	static List<TotalMessageCounts> getAllTotalMessages()
	{
		List<TotalMessageCounts> listTmc = new ArrayList<TotalMessageCounts>()

		try(Connection conn = ConnectionSource.getConnection())
		{
			final String selectSQL = "SELECT vmpd.individual_date, vmpd.username, vmpd.total_messages FROM view_messages_per_day vmpd"

			try(PreparedStatement pStatement = conn.prepareStatement(selectSQL))
			{
				try(ResultSet rSet = pStatement.executeQuery())
				{
					if(rSet.isBeforeFirst())
					{
						TotalMessageCounts tmc = new TotalMessageCounts()

						while(rSet.next())
						{
							if(tmc.date == null)
							{
								tmc.date = rSet.getDate(1).toLocalDate()
								tmc.listTotalMessages = new ArrayList<UserTotalMessages>()
								tmc.listTotalMessages.add(new UserTotalMessages(rSet.getString(2), rSet.getInt(3)))
//								tmc.listTotalMessages.add(new UserTotalMessages(rSet.getString(2), rSet.getInteger(3)))
							}
							else
							{
								if(tmc.date == rSet.getDate(1).toLocalDate())
								{
									tmc.listTotalMessages.add(new UserTotalMessages(rSet.getString(2), rSet.getInt(3)))
								}
								else
								{
									listTmc.add(tmc)
									tmc = new TotalMessageCounts()
									tmc.date = rSet.getDate(1).toLocalDate()
									tmc.listTotalMessages = new ArrayList<UserTotalMessages>()
									tmc.listTotalMessages.add(new UserTotalMessages(rSet.getString(2), rSet.getInt(3)))
								}
							}
						}

						listTmc.add(tmc)
					}
					else
					{
						System.out.println("{getAllTotalMessages}: No data returned.")
					}
				}
			}
		}
		catch(SQLException e)
		{
			e.printStackTrace()
		}

		return listTmc
	}
}