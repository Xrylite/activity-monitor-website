package activitymonitor.highcharts.postgres

import activitymonitor.highcharts.model.TimeRange
import activitymonitor.highcharts.model.TotalVoiceDurations
import activitymonitor.highcharts.model.UserVoiceDuration
import activitymonitor.postgres.ConnectionSource

import java.sql.Connection
import java.sql.PreparedStatement
import java.sql.ResultSet
import java.sql.SQLException
import java.time.LocalDateTime

class VoiceAccess
{
	static List<TotalVoiceDurations> getAllVoiceDurations()
	{
		List<TotalVoiceDurations> listTvd = new ArrayList<TotalVoiceDurations>()

		try(Connection conn = ConnectionSource.getConnection())
		{
			final String selectSQL = "SELECT vvhpd.individual_date, vvhpd.username, vvhpd.total_hours FROM view_voice_hours_per_day vvhpd"

			try(PreparedStatement pStatement = conn.prepareStatement(selectSQL))
			{
				try(ResultSet rSet = pStatement.executeQuery())
				{
					if(rSet.isBeforeFirst())
					{
						TotalVoiceDurations tvd = new TotalVoiceDurations()

						while(rSet.next())
						{
							if(tvd.date == null)
							{
								tvd.date = rSet.getDate(1).toLocalDate()
								tvd.listVoiceDurations = new ArrayList<UserVoiceDuration>()
								tvd.listVoiceDurations.add(new UserVoiceDuration(rSet.getString(2), rSet.getDouble(3)))
							}
							else
							{
								if(tvd.date == rSet.getDate(1).toLocalDate())
								{
									tvd.listVoiceDurations.add(new UserVoiceDuration(rSet.getString(2), rSet.getDouble(3)))
								}
								else
								{
									listTvd.add(tvd)
									tvd = new TotalVoiceDurations()
									tvd.date = rSet.getDate(1).toLocalDate()
									tvd.listVoiceDurations = new ArrayList<UserVoiceDuration>()
									tvd.listVoiceDurations.add(new UserVoiceDuration(rSet.getString(2), rSet.getDouble(3)))
								}
							}
						}

						listTvd.add(tvd)
					}
					else
					{
						System.out.println("{getAllVoiceDurations}: No data returned.")
					}
				}
			}
		}
		catch(SQLException e)
		{
			e.printStackTrace()
		}

		return listTvd
	}

	static List<TimeRange> getAllOfflineInstances()
	{
		List<TimeRange> listTr = new ArrayList<TimeRange>()

		try(Connection conn = ConnectionSource.getConnection())
		{
			final String selectSQL = "SELECT moi.first_run, moi.last_run FROM mv_offline_instances moi"

			try(PreparedStatement pStatement = conn.prepareStatement(selectSQL))
			{
				try(ResultSet rSet = pStatement.executeQuery())
				{
					if(rSet.isBeforeFirst())
					{
						while(rSet.next())
						{
							LocalDateTime start = rSet.getTimestamp(1).toLocalDateTime()
							LocalDateTime end = rSet.getTimestamp(2).toLocalDateTime()

							listTr.add(new TimeRange(start, end))
						}
					}
					else
					{
						System.out.println("{getAllOfflineInstances}: No data returned.")
					}
				}
			}
		}
		catch(SQLException e)
		{
			e.printStackTrace()
		}

		return listTr
	}
}