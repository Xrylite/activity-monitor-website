package activitymonitor.highcharts.model

class HCPieStructure
{
	List<HCSeries> series
	HCSeriesWrapper drilldown

	HCPieStructure()
	{

	}

	HCPieStructure(List<HCSeries> series, HCSeriesWrapper drilldown)
	{
		this.series = series
		this.drilldown = drilldown
	}
}