package activitymonitor.highcharts.model

class UserTotalMessages
{
	String userId
	String userName
	Integer totalMessages

	UserTotalMessages()
	{

	}

	UserTotalMessages(String userName, Integer totalMessages)
	{
		this.userName = userName
		this.totalMessages = totalMessages
	}

	UserTotalMessages(String userId, String userName, Integer totalMessages)
	{
		this.userId = userId
		this.userName = userName
		this.totalMessages = totalMessages
	}
}
