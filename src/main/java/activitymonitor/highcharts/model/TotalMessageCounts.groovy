package activitymonitor.highcharts.model

import java.time.LocalDate

class TotalMessageCounts
{
	LocalDate date
	List<UserTotalMessages> listTotalMessages

	TotalMessageCounts()
	{

	}

	TotalMessageCounts(LocalDate date, List<UserTotalMessages> listTotalMessages)
	{
		this.date = date
		this.listTotalMessages = listTotalMessages
	}
}