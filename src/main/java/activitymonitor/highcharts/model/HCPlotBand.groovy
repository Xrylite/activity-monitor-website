package activitymonitor.highcharts.model

import java.time.LocalDateTime

class HCPlotBand
{
	LocalDateTime from
	LocalDateTime to
	String color
	HCLabel label

	HCPlotBand()
	{

	}
}