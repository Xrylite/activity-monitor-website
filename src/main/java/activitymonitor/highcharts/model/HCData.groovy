package activitymonitor.highcharts.model

class HCData
{
	String name
	Double y
	String drilldown

	HCData()
	{

	}

	HCData(String name, Double y, String drilldown)
	{
		this.name = name
		this.y = y
		this.drilldown = drilldown
	}

	HCData(Double y)
	{
		this.y = y
	}
}
