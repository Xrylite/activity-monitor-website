package activitymonitor.highcharts.model

class TotalUserMessages
{
	UserTotalMessages utm
	List<ChannelTotalMessages> listCtm

	TotalUserMessages()
	{

	}

	TotalUserMessages(UserTotalMessages utm, List<ChannelTotalMessages> listCtm)
	{
		this.utm = utm
		this.listCtm = listCtm
	}
}
