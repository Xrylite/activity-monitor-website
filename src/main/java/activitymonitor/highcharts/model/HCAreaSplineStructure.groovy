package activitymonitor.highcharts.model

import java.time.LocalDate

class HCAreaSplineStructure
{
	List<LocalDate> dates
	List<HCSeries> series
	List<HCPlotBand> plotBands

	HCAreaSplineStructure()
	{

	}

	HCAreaSplineStructure(List<LocalDate> dates, List<HCSeries> series, List<HCPlotBand> plotBands)
	{
		this.dates = dates
		this.series = series
		this.plotBands = plotBands
	}
}