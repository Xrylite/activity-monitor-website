package activitymonitor.highcharts.model

class ChannelTotalMessages
{
	String channelId
	String channelName
	Integer totalMessages

	ChannelTotalMessages()
	{

	}

	ChannelTotalMessages(String channelId, String channelName, Integer totalMessages)
	{
		this.channelId = channelId
		this.channelName = channelName
		this.totalMessages = totalMessages
	}
}