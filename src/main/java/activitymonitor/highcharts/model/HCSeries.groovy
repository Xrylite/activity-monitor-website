package activitymonitor.highcharts.model

class HCSeries
{
	String id
	String name
	List<HCData> data

	HCSeries()
	{

	}

	HCSeries(String id, String name, List<HCData> data)
	{
		this.id = id
		this.name = name
		this.data = data
	}
}
