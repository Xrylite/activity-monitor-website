package activitymonitor.highcharts.model

class TotalChannelMessages
{
	ChannelTotalMessages ctm
	List<UserTotalMessages> listUtm

	TotalChannelMessages()
	{

	}

	TotalChannelMessages(ChannelTotalMessages ctm, List<UserTotalMessages> listUtm)
	{
		this.ctm = ctm
		this.listUtm = listUtm
	}
}
