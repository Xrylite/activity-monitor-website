package activitymonitor.highcharts.model

import java.time.LocalDate

class TotalVoiceDurations
{
	LocalDate date
	List<UserVoiceDuration> listVoiceDurations

	TotalVoiceDurations()
	{

	}

	TotalVoiceDurations(LocalDate date, List<UserVoiceDuration> listVoiceDurations)
	{
		this.date = date
		this.listVoiceDurations = listVoiceDurations
	}
}