package activitymonitor.highcharts.model

import java.time.LocalDateTime

class TimeRange
{
	LocalDateTime startTime
	LocalDateTime endTime

	TimeRange()
	{

	}

	TimeRange(LocalDateTime startTime, LocalDateTime endTime)
	{
		this.startTime = startTime
		this.endTime = endTime
	}
}