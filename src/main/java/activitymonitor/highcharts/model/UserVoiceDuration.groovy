package activitymonitor.highcharts.model

class UserVoiceDuration
{
	String userName
	Double voiceHours

	UserVoiceDuration()
	{

	}

	UserVoiceDuration(String userName, Double voiceHours)
	{
		this.userName = userName
		this.voiceHours = voiceHours
	}
}