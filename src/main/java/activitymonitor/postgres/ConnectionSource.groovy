package activitymonitor.postgres

import org.apache.commons.dbcp2.BasicDataSource
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component
import java.sql.Connection
import java.sql.SQLException

@Component
class ConnectionSource
{
	private static BasicDataSource ds = new BasicDataSource()

	private static String STATIC_URL
	private static String STATIC_USER
	private static String STATIC_PASS

	private ConnectionSource()
	{
	}

	static Connection getConnection() throws SQLException
	{
		ds.setUrl(STATIC_URL)
		ds.setUsername(STATIC_USER)
		ds.setPassword(STATIC_PASS)
		ds.setMinIdle(5)
		ds.setMaxIdle(10)
		ds.setMaxOpenPreparedStatements(100)
		return ds.getConnection()
	}

	@Value('${spring.datasource.url}')
	void setUrl(String url)
	{
		ConnectionSource.STATIC_URL = url
	}

	@Value('${spring.datasource.username}')
	void setDbUser(String dbUser)
	{
		ConnectionSource.STATIC_USER = dbUser
	}

	@Value('${spring.datasource.password}')
	void setDbPass(String dbPass)
	{
		ConnectionSource.STATIC_PASS = dbPass
	}
}