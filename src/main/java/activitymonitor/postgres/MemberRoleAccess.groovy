package activitymonitor.postgres

import java.sql.Connection
import java.sql.PreparedStatement
import java.sql.ResultSet
import java.sql.SQLException

class MemberRoleAccess
{
	static Set<String> getRolesByUserId(String userId)
	{
		try(Connection conn = ConnectionSource.getConnection()	)
		{
			final String selectSQL = "SELECT ar.role_name FROM am_member_role amr INNER JOIN am_role ar ON ar.role_id = amr.role_id WHERE amr.user_id = ?"

			try(PreparedStatement pStatement = conn.prepareStatement(selectSQL))
			{
				pStatement.setString(1, userId)

				try(ResultSet rSet = pStatement.executeQuery())
				{
					Set<String> roleSet = new HashSet<String>()

					if(rSet.isBeforeFirst())
					{
						while(rSet.next())
						{
							roleSet.add(rSet.getString(1))
						}
					}
					else
					{
						System.out.println("{getRolesByUserId}: No role data found for user id: " + userId + ".")
					}

					return roleSet
				}
			}
		}
		catch(SQLException e)
		{
			e.printStackTrace()
		}
	}
}
