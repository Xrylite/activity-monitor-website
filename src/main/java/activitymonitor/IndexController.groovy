package activitymonitor

import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.GetMapping

import javax.servlet.http.HttpServletRequest

@Controller
class IndexController
{
	@GetMapping("/")
	String index(Model model, HttpServletRequest request, OAuth2AuthenticationToken oa)
	{
		// Authorities: oa?.getAuthorities()
		System.out.println("[index]: " + request.getRemoteAddr())

		return "index"
	}

	@GetMapping("/messages")
	String messages(Model model, HttpServletRequest request, OAuth2AuthenticationToken oa)
	{
		// Authorities: oa?.getAuthorities()
		System.out.println("[messages]: " + request.getRemoteAddr())

		return "messages"
	}

	@GetMapping("/voice")
	String voice(Model model, HttpServletRequest request, OAuth2AuthenticationToken oa)
	{
		// Authorities: oa?.getAuthorities()
		System.out.println("[voice]: " + request.getRemoteAddr())

		return "voice"
	}

	@GetMapping("/activity")
	String activity(Model model, HttpServletRequest request, OAuth2AuthenticationToken oa)
	{
		// Authorities: oa?.getAuthorities()
		System.out.println("[activity]: " + request.getRemoteAddr())

		return "activity"
	}

	@GetMapping("/settings")
	String settings(Model model, HttpServletRequest request, OAuth2AuthenticationToken oa)
	{
		// Authorities: oa?.getAuthorities()
		System.out.println("[settings]: " + request.getRemoteAddr())

		return "settings"
	}

	@GetMapping("/signout")
	String signout(Model model, HttpServletRequest request)
	{
		System.out.println("[signout]: " + request.getRemoteAddr())
		request.getSession().invalidate()

		return "redirect:/"
	}

	@GetMapping("/error")
	String error(Model model, HttpServletRequest request)
	{
		System.out.println("[error]: " + request.getRemoteAddr())
		return "error"
	}
}