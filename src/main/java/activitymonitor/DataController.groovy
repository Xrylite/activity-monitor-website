package activitymonitor

import activitymonitor.highcharts.logic.TotalChannelMessages
import activitymonitor.highcharts.logic.TotalUserMessages
import activitymonitor.highcharts.logic.TotalVoice
import activitymonitor.highcharts.model.HCAreaSplineStructure
import activitymonitor.highcharts.model.HCPieStructure
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseBody

import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Controller
class DataController
{
	@RequestMapping("/totalMessagesForAllChannels")
	@ResponseBody
	HCPieStructure totalMessagesForAllChannels(HttpServletRequest request, HttpServletResponse response, Model model)
	{
		HCPieStructure hcps = TotalChannelMessages.getTotalMessagesForAllChannels()

//		ObjectMapper mapper = new ObjectMapper();
//		String jsonString = mapper.writeValueAsString(tcm);

		return hcps
	}

	@RequestMapping("/totalMessagesForAllUsers")
	@ResponseBody
	HCPieStructure totalMessagesForAllUsers(HttpServletRequest request, HttpServletResponse response, Model model)
	{
		HCPieStructure hcps = TotalUserMessages.getTotalMessagesForAllUsers()

		return hcps
	}

	@RequestMapping("/messagesByServer")
	@ResponseBody
	HCAreaSplineStructure messagesByServer(HttpServletRequest request, HttpServletResponse response, Model model)
	{
		HCAreaSplineStructure hcass = TotalUserMessages.getTotalMessagesForServer()

		return hcass
	}

	@RequestMapping("/voiceByServer")
	@ResponseBody
	HCAreaSplineStructure voiceByServer(HttpServletRequest request, HttpServletResponse response, Model model)
	{
		HCAreaSplineStructure hcass = TotalVoice.getTotalVoiceForAllUsers()

		return hcass
	}
}